<?php

namespace Radenmasgalih\Alacarte\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class UninstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alacarte:uninstall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uninstall the Alacarte';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->uninstallEngine();
        $this->uninstallXoric();
    }

    protected function uninstallEngine()
    {
        // Remove Controllers...
        (new Filesystem)->ensureDirectoryExists(app_path('Http/Controllers/Auth'));
        (new Filesystem)->deleteDirectory(app_path('Http/Controllers/Auth'));

        // Requests...
        (new Filesystem)->ensureDirectoryExists(app_path('Http/Requests/Auth'));
        (new Filesystem)->deleteDirectory(app_path('Http/Requests/Auth'));

        // Components...
        (new Filesystem)->ensureDirectoryExists(app_path('View/Components'));
        (new Filesystem)->deleteDirectory(app_path('View/Components'));

        // Tests...
        (new Filesystem)->ensureDirectoryExists(base_path('tests/Feature'));
        (new Filesystem)->delete(base_path('tests/Feature/AuthenticationTest.php'));
        (new Filesystem)->delete(base_path('tests/Feature/EmailVerificationTest.php'));
        (new Filesystem)->delete(base_path('tests/Feature/PasswordConfirmationTest.php'));
        (new Filesystem)->delete(base_path('tests/Feature/PasswordResetTest.php'));
        (new Filesystem)->delete(base_path('tests/Feature/RegistrationTest.php'));

        // Routes...
        (new Filesystem)->delete(base_path('routes/web.php'));
        (new Filesystem)->delete(base_path('routes/auth.php'));
        copy(__DIR__ . '/../../stubs/routes/webDefault.php', base_path('routes/web.php'));

        // Back to default "Home" Route...
        $this->replaceInFile('/dashboard', '/home', app_path('Providers/RouteServiceProvider.php'));

        $this->info('Auth Engine uninstall successfully.');
    }

    protected function uninstallXoric()
    {
        $this->comment("Please wait and don't close the terminal while deleting file");
        //ASSETS
        //CSS
        (new Filesystem)->ensureDirectoryExists(public_path('css'));
        (new Filesystem)->deleteDirectory(public_path('css'));

        //JS
        (new Filesystem)->ensureDirectoryExists(public_path('js'));
        (new Filesystem)->deleteDirectory(public_path('js'));

        //FONTS
        (new Filesystem)->ensureDirectoryExists(public_path('fonts'));
        (new Filesystem)->deleteDirectory(public_path('fonts'));

        //LIBS
        (new Filesystem)->ensureDirectoryExists(public_path('libs'));
        (new Filesystem)->deleteDirectory(public_path('libs'));

        //IMAGES
        (new Filesystem)->ensureDirectoryExists(public_path('images'));
        (new Filesystem)->deleteDirectory(public_path('images'));

        // Views...
        (new Filesystem)->ensureDirectoryExists(resource_path('views/auth'));
        (new Filesystem)->ensureDirectoryExists(resource_path('views/layouts'));
        (new Filesystem)->ensureDirectoryExists(resource_path('views/components'));

        (new Filesystem)->deleteDirectory(resource_path('views/auth'));
        (new Filesystem)->deleteDirectory(resource_path('views/layouts'));
        (new Filesystem)->deleteDirectory(resource_path('views/components'));

        (new Filesystem)->delete(resource_path('views/dashboard.blade.php'));

        $this->info('Xoric Template uninstalled successfully.');
    }

    protected function replaceInFile($search, $replace, $path)
    {
        file_put_contents($path, str_replace($search, $replace, file_get_contents($path)));
    }
}
