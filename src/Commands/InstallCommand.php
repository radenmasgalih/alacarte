<?php

namespace Radenmasgalih\Alacarte\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alacarte:install {template}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Alacarte';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->installEngine();

        if ($this->option() == 'xoric') {
            $this->installXoric();
        }
    }

    protected function installEngine()
    {
        // Controllers...
        (new Filesystem)->ensureDirectoryExists(app_path('Http/Controllers/Auth'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/App/Http/Controllers/Auth', app_path('Http/Controllers/Auth'));

        // Requests...
        (new Filesystem)->ensureDirectoryExists(app_path('Http/Requests/Auth'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/App/Http/Requests/Auth', app_path('Http/Requests/Auth'));

        // Components...
        (new Filesystem)->ensureDirectoryExists(app_path('View/Components'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/App/View/Components', app_path('View/Components'));

        // Tests...
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/tests/Feature', base_path('tests/Feature'));

        // Routes...
        copy(__DIR__ . '/../../stubs/routes/web.php', base_path('routes/web.php'));
        copy(__DIR__ . '/../../stubs/routes/auth.php', base_path('routes/auth.php'));

        // "Dashboard" Route...
        $this->replaceInFile('/home', '/dashboard', app_path('Providers/RouteServiceProvider.php'));

        $this->info('Auth Engine installed successfully.');
    }

    protected function installXoric()
    {
        $this->comment("Please wait and don't close the terminal while copying file");

        //ASSETS
        // CSS
        (new Filesystem)->ensureDirectoryExists(public_path('css'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../resources/xoric/css', public_path('css'));

        //JS
        (new Filesystem)->ensureDirectoryExists(public_path('js'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../resources/xoric/js', public_path('js'));

        //FONTS
        (new Filesystem)->ensureDirectoryExists(public_path('fonts'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../resources/xoric/fonts', public_path('fonts'));

        //LIBS
        (new Filesystem)->ensureDirectoryExists(public_path('libs'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../resources/xoric/libs', public_path('libs'));

        //IMAGES
        (new Filesystem)->ensureDirectoryExists(public_path('images'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../resources/xoric/images', public_path('images'));

        // Views...
        (new Filesystem)->ensureDirectoryExists(resource_path('views/auth'));
        (new Filesystem)->ensureDirectoryExists(resource_path('views/layouts'));
        (new Filesystem)->ensureDirectoryExists(resource_path('views/components'));

        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/resources/xoric/views/auth', resource_path('views/auth'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/resources/xoric/views/layouts', resource_path('views/layouts'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/resources/xoric/views/components', resource_path('views/components'));

        copy(__DIR__ . '/../../stubs/resources/xoric/views/dashboard.blade.php', resource_path('views/dashboard.blade.php'));
        $this->replaceInFile('/home', '/dashboard', app_path('Providers/RouteServiceProvider.php'));

        $this->info('Xoric Template installed successfully.');
    }

    protected function replaceInFile($search, $replace, $path)
    {
        file_put_contents($path, str_replace($search, $replace, file_get_contents($path)));
    }
}
