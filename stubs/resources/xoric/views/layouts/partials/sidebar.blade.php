<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('dashboard') }}" class="waves-effect {{ request()->segment(1) == 'dashboard' ? 'mm-active' : '' }}">
                        <div class="d-inline-block icons-sm mr-1"><i class="uim uim-airplay"></i></div>
                        <span>Dashboard</span>
                    </a>
                </li>

                {{-- <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <div class="d-inline-block icons-sm mr-1"><i class="uim uim-align-left-justify"></i></div>
                        <span>Menu</span>
                    </a>
                    <ul class="sub-menu {{ (request()->segment(1) == 'menu_1' OR request()->segment(1) == 'menu_2') ? 'mm-show' : '' }}" aria-expanded="false">
                <li><a href="{{ route('menu_1.index') }}" class="{{ request()->segment(1) == 'menu_1' ? 'mm-active' : '' }}">Menu 1</a></li>
                <li><a href="{{ route('menu_2.index') }}" class="{{ request()->segment(1) == 'menu_2' ? 'mm-active' : '' }}">Menu 2</a></li>
            </ul>
            </li> --}}


            </ul>

        </div>
    </div>
</div>
