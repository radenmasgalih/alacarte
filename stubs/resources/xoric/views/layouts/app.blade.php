<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">

    <!-- datepicker -->
    <link href="{{ asset('libs/air-datepicker/css/datepicker.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('site/css/iconcrafts.css') }}">
    <!-- DataTables -->
    <link href="{{ asset('libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{ asset('libs/summernote/summernote-bs4.css') }}" rel="stylesheet" type="text/css" />
    <!-- alertifyjs and swal Css -->
    <link href="{{ asset('libs/alertifyjs/build/css/alertify.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('libs/alertifyjs/build/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link href="{{ asset('libs/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Lightbox css -->
    <link href="{{ asset('libs/magnific-popup/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <!-- file upload -->
    <link href="{{ asset('libs/fileUpload/css/fileinput.css') }}" rel="stylesheet">
    <!-- nestable2 css -->
    <link href="{{ asset('libs/nestable2/jquery.nestable.min.css') }}" rel="stylesheet">
    <!-- bootstrap toggle -->
    <link href="{{ asset('libs/bootstrap-toggle-master/css/bootstrap-toggle.css') }}" rel="stylesheet">
    <!-- pace-progress -->
    <link href="{{ asset('libs/pace-progress/themes/red/pace-theme-flat-top.css') }}" rel="stylesheet">
    <!-- App Css-->
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- alacarte css -->
    <link href="{{ asset('css/alacarte.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
</head>

<body data-topbar="colored">
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- Top Bar Start -->
        @include('layouts.partials.topbar')
        <!-- Top Bar End -->

        <!-- ========== Left Sidebar Start ========== -->
        @include('layouts.partials.sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">
            <!-- Start content -->
            <div class="page-content">

                <div class="page-title-box">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                {{ $header }}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end row -->

                <div class="page-content-wrapper">
                    <div class="container-fluid">
                        {{ $slot }}
                    </div>
                </div>
                <!-- end page content-->
            </div> <!-- content -->

            @include('layouts.partials.footer')

        </div>
    </div>
    <!-- END wrapper -->

    <!-- JAVASCRIPT -->
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('libs/node-waves/waves.min.js') }}"></script>

    <script src="{{ asset('libs/unicons/unicons.js') }}"></script>

    <!-- datepicker -->
    <script src="{{ asset('libs/air-datepicker/js/datepicker.min.js') }}"></script>
    <script src="{{ asset('libs/air-datepicker/js/i18n/datepicker.en.js') }}"></script>
    <!-- datatable js -->
    <script src="{{ asset('libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- datatable Responsive examples -->
    <script src="{{ asset('libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- Moment js -->
    <script src="{{ asset('libs/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('libs/moment/locale/id.js') }}"></script>
    <!-- Summernote js -->
    <script src="{{ asset('libs/summernote/summernote-bs4.min.js') }}"></script>
    <!-- Magnific Popup-->
    <script src="{{ asset('libs/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <!-- nestable2 js -->
    <script src="{{ asset('libs/nestable2/jquery.nestable.min.js') }}"></script>
    <!-- File Upload -->
    <script src="{{ asset('libs/fileUpload/js/fileinput.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('libs/select2/js/select2.full.min.js') }}"></script>
    <!-- alertify and swal-->
    <script src="{{ asset('libs/alertifyjs/build/alertify.min.js') }}"></script>
    <script src="{{ asset('libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/alert.js') }}"></script>
    <!-- Price Format -->
    <script src="{{ asset('js/jquery.price_format.2.0.js') }}"></script>
    <!-- pace-progress -->
    <script src="{{ asset('libs/pace-progress/pace.min.js') }}"></script>
    <!-- nestable2 js -->
    <script src="{{ asset('libs/nestable2/jquery.nestable.min.js') }}"></script>
    <!-- Jquery Number -->
    <script src="{{ asset('js/jquery.number.js') }}"></script>
    <!-- Bootstrap Toggle -->
    <script src="{{ asset('libs/bootstrap-toggle-master/js/bootstrap-toggle.js') }}"></script>
    <script src="{{ asset('js/app2.js') }}"></script>
    <script src="{{ asset('js/alacarte.js') }}"></script>
</body>

</html>
