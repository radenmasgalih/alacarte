<x-auth-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">
                        <h5 class="mb-5 text-center">Register Account to Xoric.</h5>

                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Name -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="text" class="form-control" id="name" name="name" required autofocus>
                                        <label for="name">Username</label>
                                    </div>

                                    <!-- Email Address -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="email" class="form-control" id="email" name="email" required>
                                        <label for="email">Email</label>
                                    </div>

                                    <!-- Password -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="password" class="form-control" id="password" name="password" required autocomplete="new-password">
                                        <label for="password">Password</label>
                                    </div>

                                    <!-- Confirm Password -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                                        <label for="password_confirmation">Confirm Password</label>
                                    </div>

                                    <div class="mt-4">
                                        <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Register</button>
                                    </div>

                                    <div class="mt-4 text-center">
                                        <a href="{{ route('login') }}" class="text-muted"><i class="mdi mdi-account-circle mr-1"></i> Sudah punya akun ?</a>
                                        <p class="text-muted mt-3">© 2020 Jenderal Software</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-auth-layout>
