<x-auth-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">
                        <h5 class="mb-5 text-center">Create New Password</h5>

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <form class="form-horizontal" method="POST" action="{{ route('password.update') }}">
                            @csrf

                            <!-- Password Reset Token -->
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">

                            <!-- Email Address -->
                            <div class="form-group form-group-custom mb-4">
                                <input type="email" class="form-control" id="email" name="email" required autofocus>
                                <label for="email">Email</label>
                            </div>

                            <!-- Password -->
                            <div class="form-group form-group-custom mb-4">
                                <input type="password" class="form-control" id="password" name="password" required autocomplete="new-password">
                                <label for="password">Password</label>
                            </div>

                            <!-- Confirm Password -->
                            <div class="form-group form-group-custom mb-4">
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                                <label for="password_confirmation">Confirm Password</label>
                            </div>

                            <div class="mt-4">
                                <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Reset Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-auth-layout>
