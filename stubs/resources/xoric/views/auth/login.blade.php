<x-auth-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">
                        <h5 class="mb-4 text-center">Sign in to continue</h5>

                        <!-- Session Status -->
                        <x-auth-session-status class="alert alert-danger mb-4" :status="session('status')" />

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="alert alert-danger mb-4" :errors="$errors" />

                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Email Address -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="email" class="form-control" id="email" name="email" required autofocus>
                                        <label for="email">Email</label>
                                    </div>

                                    <!-- Password -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="password" class="form-control" id="password" name="password" required autocomplete="current-password">
                                        <label for="password">Password</label>
                                    </div>

                                    <!-- Remember Me -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="remember_me" name="remember">
                                                <label class="custom-control-label" for="remember_me">Remember me</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-md-right mt-3 mt-md-0">
                                                @if (Route::has('password.request'))
                                                <a class="text-muted" href="{{ route('password.request') }}">
                                                    <i class="mdi mdi-lock"></i> Forgot your password?
                                                </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mt-4">
                                        <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Log In</button>
                                    </div>

                                    <div class="mt-4 text-center">
                                        <a href="{{ route('register') }}" class="text-muted"><i class="mdi mdi-account-circle mr-1"></i> Daftar Sekarang</a>
                                        <p class="text-muted mt-3">© 2021 Jenderal Software</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-auth-layout>
