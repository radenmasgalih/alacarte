<x-autentikasi-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">

                        <h5 class="mb-4 text-center">Reset Password</h5>

                        <div class="alert alert-info mb-4" role="alert">
                            Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.
                        </div>


                        @if (session('status') == 'verification-link-sent')
                        <div class="alert alert-info mb-4" role="alert">
                            A new verification link has been sent to the email address you provided during registration.
                        </div>
                        @endif

                        <form method="POST" action="{{ route('verification.send') }}">
                            @csrf

                            <div>
                                <x-button>
                                    {{ __('Resend Verification Email') }}
                                </x-button>
                            </div>
                        </form>

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <button type="submit" class="btn btn-danger btn-block waves-effect waves-light">
                                Logout
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-autentikasi-layout>
