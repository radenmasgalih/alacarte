<x-auth-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">

                        <h5 class="mb-4 text-center">Reset Password</h5>

                        <div class="alert alert-info mb-4" role="alert">
                            Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
                        </div>

                        <!-- Session Status -->
                        <x-auth-session-status class="alert alert-danger mb-4" :status="session('status')" />

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="alert alert-danger mb-4" :errors="$errors" />

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Email Address -->
                                    <div class="form-group form-group-custom mb-4">
                                        <input type="email" class="form-control" id="email" name="email" required autofocus>
                                        <label for="email">Email</label>
                                    </div>

                                    <div class="mt-4">
                                        <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Send Email</button>
                                    </div>

                                    <div class="mt-4 text-center">
                                        <a href="{{ route('login') }}" class="text-muted"><i class="mdi mdi-account-circle mr-1"></i> Login</a>
                                        <p class="text-muted mt-3">© 2020 Jenderal Software</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-auth-layout>
