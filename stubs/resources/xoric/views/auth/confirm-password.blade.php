<x-autentikasi-layout>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-sm-8">
            <div class="card">
                <div class="card-body p-4">
                    <div class="p-2">
                        <h5 class="mb-5 text-center">Confirm Password</h5>

                        <div class="alert alert-info mb-4" role="alert">
                            This is a secure area of the application. Please confirm your password before continuing.
                        </div>

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <form class="form-horizontal" method="POST" action="{{ route('password.confirm') }}">
                            @csrf

                            <!-- Password -->
                            <div class="form-group form-group-custom mb-4">
                                <input type="password" class="form-control" id="password" name="password" required autocomplete="current-password">
                                <label for="password">Password</label>
                            </div>

                            <div class="mt-4">
                                <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-autentikasi-layout>
