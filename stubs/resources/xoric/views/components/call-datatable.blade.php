@props(['value'])

<script>
    $(document).ready(function () {
        $("{{ $value ?? $slot }}").DataTable({
            responsive: true
        });
    });
</script>
