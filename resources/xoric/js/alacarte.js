//Initialize Select2 Elements
$(".select2").select2();

//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
	placeholder: "Pilih",
});

$(document).ready(function () {
	$(".summernote").summernote({
		height: 500, // set editor height
		minHeight: null, // set minimum height of editor
		maxHeight: null, // set maximum height of editor
		toolbar: [
			// [groupName, [list of button]]
			["style", ["style"]],
			["style", ["bold", "italic", "underline", "clear"]],
			["font", ["strikethrough", "superscript", "subscript"]],
			["fontsize", ["fontsize"]],
			["color", ["color"]],
			["para", ["ul", "ol", "paragraph"]],
			["height", ["height"]],
			["fontname", ["fontname"]],
			["table", ["table"]],
			["insert", ["link", "picture", "video"]],
			["view", ["fullscreen", "codeview", "help"]],
		],
	});
});

//Date picker
$(".datepicker").datepicker({
	autoclose: true,
	format: "yyyy-mm-dd",
});

$(".monthpicker").datepicker({
	autoclose: true,
});

$(function () {
	$(".photo-input").fileinput({
		browseLabel: "Select Photo",
		browseClass: "btn btn-info",
		browseIcon: '<i class="fa fa-picture-o"></i>',
		removeLabel: "Cancel",
		removeClass: "btn btn-danger",
		removeIcon: '<i class="fa fa-times-circle"></i>',
		layoutTemplates: {
			icon: '<i class="fa fa-file-image-o"></i>',
		},
		showUpload: false,
		showClose: false,
		maxFilesNum: 10,
		allowedFileExtensions: ["jpg", "png", "jpeg", "gif"],
		overwriteInitial: true,
		browseOnZoneClick: true,
	});
});

$(function () {
	$(".file-input").fileinput({
		browseLabel: "Select File",
		browseClass: "btn btn-info",
		browseIcon: '<i class="fa fa-file-o"></i>',
		removeLabel: "Cancel",
		removeClass: "btn btn-danger",
		removeIcon: '<i class="fa fa-times-circle"></i>',
		layoutTemplates: {
			icon: '<i class="fa fa-file-image-o"></i>',
		},
		showUpload: false,
		showClose: false,
		maxFilesNum: 10,
		allowedFileExtensions: [
			"txt",
			"pdf",
			"docx",
			"xlsx",
			"pptx",
			"rtf",
			"rar",
			"zip",
			"doc",
			"xls",
			"ppt",
			"jpg",
			"png",
			"jpeg",
			"gif",
		],
		overwriteInitial: true,
		initialPreviewAsData: true,
		browseOnZoneClick: true,
	});
});

$(function () {
	$(".file-input-update").fileinput({
		browseLabel: "Ubah File",
		browseClass: "btn btn-info",
		browseIcon: '<i class="fa fa-file-o"></i>',
		removeLabel: "Cancel",
		removeClass: "btn btn-danger",
		removeIcon: '<i class="fa fa-times-circle"></i>',
		layoutTemplates: {
			icon: '<i class="fa fa-file-image-o"></i>',
		},
		showUpload: false,
		showClose: false,
		maxFilesNum: 10,
		allowedFileExtensions: [
			"txt",
			"pdf",
			"docx",
			"xlsx",
			"pptx",
			"rtf",
			"rar",
			"zip",
			"doc",
			"xls",
			"ppt",
			"jpg",
			"png",
			"jpeg",
			"gif",
		],
		overwriteInitial: true,
		dropZoneEnabled: false,
		initialCaption: "1 File Terunggah",
	});
});

$(".uang").priceFormat({
	prefix: "Rp  ",
	thousandsSeparator: ".",
	centsLimit: 0,
});

//lightbox
$(
	function (e) {
		e(".lightbox").magnificPopup({
			type: "image",
			closeOnContentClick: !0,
			closeBtnInside: !1,
			fixedContentPos: !0,
			mainClass: "mfp-no-margins mfp-with-zoom",
			image: { verticalFit: !0 },
			zoom: { enabled: !0, duration: 300 },
		});
	}.apply(this, [jQuery])
);

$(".jNumber").number(true);
